Ansible Heartbeat Role
======================

Ansible role for Heartbeat installation and configuration.  
The only supported target system is Linux.  
The only supported option for service creation is systemd.  
The only supported option for installation is rpm file.  

TODO
----

Support for installation from zip.
Support for other service systems.

Requirements
------------

pexpect>=3.3 must be installed on target machine  
Heartbeat installation artifact (rpm)  

Role Variables
--------------

**env**: Environment variable, default dev  
**heartbeat_download**: Defines if heartbeat should be downloaded from heartbeat_download_url, default True, set to False if artifact is already present on disk  
**heartbeat_version**: heartbeat version, default 6.6.2  
**heartbeat_ext**: heartbeat artifact extension, default rpm  
**heartbeat_download_url**: Download url containg heartbeat artifact  
**heartbeat_download_user**: User to authenticate at download url, should be overriden on playbook level  
**heartbeat_download_password**: Password to authenticate at download url, should be overriden on playbook level  
**heartbeat_download_dest**: Destination to download heartbeat artifact to, default /tmp/heartbeat-version.rpm 
**heartbeat_create_user**: Flag controling wether user and group is created  
**heartbeat_secure_user**: Block ssh login for heartbeat user  
**heartbeat_user**: Heartbeat user that will be used by service and set as owner of files, default heartbeat  
**heartbeat_group**: Heartbeat group that will be set on files, default heartbeat  
**heartbeat_create_configuration**: Create configuration from templates, default True
**heartbeat_service_enabled**: Service enabled setting, default yes  
**heartbeat_service_status**: Service status setting, default started  
**heartbeat_service_name**: Service name, default heartbeat-elastic  
**heartbeat_config_path**: Heartbeat config path, default /etc/heartbeat  
**heartbeat_data_path**: Heartbeat data path, default /var/lib/heartbeat  
**heartbeat_log_path**: Heartbeat log path, default /var/log/heartbeat  
**heartbeat_reload_config**: Heartbeat reload config setting, default yes  
**heartbeat_reload_period**: Heartbeat reload config frequency, default 60s  
**heartbeat_logstash_url**: Logstash url
**heartbeat_key_value**: Heartbeat key value in secret vault, should be overriden on playbook level  
**heartbeat_create_healthchecks**: Create healthcheck config from template, default True

Example Playbook
----------------


    - hosts: servers
      gather_facts: False
      tasks:
        - import_role:
            name: ansible-role-heartbeat
          vars:
            heartbeat_download_user: arti_npa_user
            heartbeat_download_password: "{{ vault_arti_password }}"
            heartbeat_key_value: "{{ vault_heartbeat_key_value }}"
